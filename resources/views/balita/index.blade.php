@extends('app-layouts.admin.index')

@section('content')
<div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Data Balita</h1>
      </div>
      @include('includes.admin.alert')
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header d-inline">
              <h4>Data Posyandu</h4>
              <a href="/dashboard/balita/create" class=" btn btn-primary mt-3">Tambah Data Balita</a>
              <a href="#" class=" btn btn-primary mt-3">Unduh Data</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                  <thead>
                    <tr>
                      <th class="text-center">
                        No
                      </th>
                      <th>Nama</th>
                      <th>Nama Ibu</th>
                      <th>Umur</th>
                      <th>Jenis Kelamin</th>
                      <th>Tanggal Lahir</th>
                      <th>Alamat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse ($data as $item)
                  <tr>
                    <th>{{ $loop->iteration}}</th>
                    <th>{{ $item->nama_balita }}</th>
                    <th>{{ $item->nama_ibu }}</th>
                    <th>
                      {{ $item->umur_tahun }} tahun {{ $item->umur_bulan }} bulan
                    </th>
                    <th>{{ $item->jenis_kelamin }}</th>
                    <th>{{ $item->tgl_lahir}}</th>
                    <th>{{ $item->alamat }}</th>
                    <td>
                        <a href="/dashboard/balita/{{ $item->id }}/edit" class="btn btn-info">
                        <i class="fa fa-pencil-alt"></i>
                        </a>
                        <a href="{{ route('balita.destroy', $item->id) }}" class="btn btn-danger" data-confirm-delete="true">
                          <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                @empty
                    <tr>
                      <td colspan="8" class="text-center">
                          No data avaliable in table
                      </td>
                    </tr>
                  @endforelse
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="section-body">
      </div>
    </section>
  </div>
@endsection
