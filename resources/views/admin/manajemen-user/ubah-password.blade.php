@extends('admin.layout')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Ubah Password</h1>
            </div>
            <div class="section-body">
                <h2 class="section-title">Form Ubah Password User</h2>
                @if(session('add_err'))
                    <div class="alert alert-danger">
                        {{ session('add_err') }}
                    </div>
                @endif
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <form action="{{ route('user.changePwdStore', [
                                'id' => $data->id
                            ]) }}" method="POST">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="nama">Nama</label>
                                        <input type="text" class="form-control-plaintext" value="{{ $data->name }}" id="nama" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control-plaintext" value="{{ $data->email }}" id="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="password">Password Baru</label>
                                        <input type="text" class="form-control @error('password') is-invalid @enderror" name="password" id="password" placeholder="password" value="{{ old('password') }}">
                                        @error('password')
                                            <div class="invalid-feedback">
                                            {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="card-footer text-right">
                                <button class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection