@extends('admin.layout')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Data Users</h1>
            </div>
            <div class="row">
                <div class="col-12">
                    @if(session('add_success'))
                        <div class="alert alert-success">
                            {{ session('add_success') }}
                        </div>
                    @endif
                    @if(session('changePwd_success'))
                        <div class="alert alert-success">
                            {{ session('changePwd_success') }}
                        </div>
                    @endif
                    @if(session('delete_err'))
                        <div class="alert alert-danger">
                            {{ session('delete_err') }}
                        </div>
                    @endif
                    @if(session('delete_success'))
                        <div class="alert alert-success">
                            {{ session('delete_success') }}
                        </div>
                    @endif
                    <div class="card">
                        <div class="card-header d-inline">
                            <h4>Data Users</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table-1">
                                    <thead>
                                        <tr>
                                        <th class="text-center">
                                            No
                                        </th>
                                        <th>Nama</th>
                                        <th>Email</th>
                                        <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($data as $item)
                                            <tr>
                                                <th class="text-center">{{ $loop->iteration}}</th>
                                                <th>{{ $item->name }}</th>
                                                <th>{{ $item->email }}</th>
                                                <td>
                                                    <form action="{{ route('user.delete', [
                                                        'id' => $item->id
                                                    ]) }}" method="POST" style="display: inline-block">
                                                        @csrf
                                                        @method('delete')
                                                        <button class="btn btn-danger" type="submit">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </form>
                                                    <a href="{{ route('user.changePwd', [
                                                        'id' => $item->id
                                                    ]) }}" class="btn btn-info">
                                                        <i class="fa fa-pencil-alt"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
