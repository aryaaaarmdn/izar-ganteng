<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="/">Posyandu Melati 04</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">St</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li>
                <a class="nav-link" href="{{route('admin.index')}}">
                    <i class="fas fa-fire"></i> 
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="menu-header">Manajemen User</li>
            <li>
                <a class="nav-link" href="{{ route('user.index') }}">
                    <i class="fas fa-fire"></i> 
                    <span>Data Users</span>
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('user.add') }}">
                    <i class="fas fa-fire"></i> 
                    <span>Tambah User</span>
                </a>
            </li>
        </ul>
    </aside>
  </div>