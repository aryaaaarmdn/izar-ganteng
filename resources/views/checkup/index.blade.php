@extends('app-layouts.admin.index')

@section('content')
<div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Data Checkup Balita</h1>
      </div>
      @include('includes.admin.alert')
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header d-inline">
              <h4>Data Posyandu</h4>
              <a href="/dashboard/checkup/create" class=" btn btn-primary mt-3">Tambah Data Checkup</a>
              <a href="#" class=" btn btn-primary mt-3">Unduh Data</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                  <thead>
                    <tr>
                      <th class="text-center">
                        No
                      </th>
                      <th>Nama Balita</th>
                      <th>Berat Badan</th>
                      <th>Vitamin</th>
                      <th>Imunisasi</th>
                      <th>Status Gizi</th>
                      <th>Tanggal Imunisasi</th>
                      <th>Checkup Ke</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($data as $item)
                  <tr>
                    <th>{{ $loop->iteration}}</th>
                      <th>{{ $item->balita->nama_balita }}</th>
                      <th>{{ $item->berat_badan }}</th>
                      <th>{{ $item->vitamin->jenis_vitamin }}</th>
                      <th>{{ $item->imunisasi->jenis_imunisasi }}</th>
                      <th>{{ $item->status_gizi }}</th>
                      <th>{{ \Carbon\Carbon::parse($item->created_at)->translatedFormat('d F Y') }}</th>
                      <th>{{ $item->checkup_ke }}</th>
                    <td>
                        <a href="/dashboard/checkup/{{ $item->id }}/edit" class="btn btn-info">
                        <i class="fa fa-pencil-alt"></i>
                        </a>
                        <a href="{{ route('checkup.destroy', $item->id) }}" class="btn btn-danger" data-confirm-delete="true">
                          <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="section-body">
      </div>
    </section>
  </div>
@endsection
