@extends('app-layouts.admin.index')

@section('content')
<div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Data Ibu Hamil</h1>
      </div>
      @include('includes.admin.alert')
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header d-inline">
              <h4>Data Posyandu</h4>
              <a href="/dashboard/bumil/create" class=" btn btn-primary mt-3">Tambah Data Ibu Hamil</a>
              <a href="#" class=" btn btn-primary mt-3">Unduh Data</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                  <thead>
                    <tr>
                      <th class="text-center">
                        No
                      </th>
                      <th>Nama Ibu</th>
                      <th>NIK</th>
                      <th>Umur</th>
                      <th>Alamat</th>
                      <th>Masa Kehamilan</th>
                      <th>Checkup Ke</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($data as $item)
                  <tr>
                    <th class="text-center">{{ $loop->iteration}}</th>
                    <th>{{ $item->nama_ibu}}</th>
                    <th>{{ $item->NIK }}</th>
                    <th>
                      {{ $item->umur_tahun }} tahun {{ $item->umur_bulan }} bulan
                    </th>
                    <th>{{ $item->alamat }}</th>
                    <th>{{ $item->masa_kehamilan }}</th>
                    <th>{{ $item->checkup_ke }}</th>
                    <td>
                        <a href="/dashboard/bumil/{{ $item->id }}/edit" class="btn btn-info">
                        <i class="fa fa-pencil-alt"></i>
                        </a>
                        <a href="{{ route('bumil.destroy', $item->id) }}" class="btn btn-danger" data-confirm-delete="true">
                          <i class="fa fa-trash"></i>
                        </a>
                    </td>
                </tr>
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="section-body">
      </div>
    </section>
  </div>
@endsection
