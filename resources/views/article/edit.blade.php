@extends('app-layouts.admin.index')

@section('content')
<div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Update Data</h1>
      </div>
      <div class="section-body">
        <h2 class="section-title">Form Input Data Informasi Kegiatan</h2>

        <div class="row">
          <div class="col-12">
            <div class="card">
                <form action="/dashboard/article/{{ $data->id }}" method="POST" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="card-header">
                  <h4>Masukkan Data Kegiatan Dengan Sesuai</h4>
                </div>
                <div class="card-body">
                    <div class="form-group row mb-4">
                      <label for="title" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Judul</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" placeholder="Judul" value="{{ $data->title}}">
                      </div>
                      @error('title')
                            <div class="invalid-feedback">
                            {{ $message }}
                            </div>
                      @enderror
                    </div>
                    <div class="form-group row mb-4">
                      <label for="body" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Content</label>
                      <div class="col-sm-12 col-md-7">
                        <textarea name="body" id="body" class="summernote @error('body') is-invalid @enderror">{{ $data->body }}</textarea>
                      </div>
                      @error('body')
                            <div class="invalid-feedback">
                            {{ $message }}
                            </div>
                      @enderror
                    </div>
                    <div class="form-group row mb-4">
                        <label for="image" class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Gambar</label>
                        <input class="form-control" type="hidden" name="oldImage" value="{{ $data->image }}">
                        <div class="col-sm-12 col-md-7">
                          <input type="file" class="form-control  @error('image') is-invalid @enderror" name="image" placeholder="image" value="{{ $data->image }}">
                        </div>
                      @error('image')
                            <div class="invalid-feedback">
                            {{ $message }}
                            </div>
                      @enderror
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                      <div class="col-sm-12 col-md-7">
                        <button class="btn btn-primary">Publish</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection
