@extends('app-layouts.index')

@section('content')
    <div class="container mt-4">
        <div class="card">
            <img src="{{ asset('storage/'. $data->image) }}" 
                class="card-img-top img-fluid" alt="..."
            >
            <div class="card-body">
              <h5 class="card-title">{{ $data->title }}</h5>
              <p class="card-text">{{ $data->body }}</p>
              <small class="text-muted">
                Created at: {{ \Carbon\Carbon::parse($data->created_at)->translatedFormat('d F Y') }}
              </small>
            </div>
          </div>
    </div>
@endsection