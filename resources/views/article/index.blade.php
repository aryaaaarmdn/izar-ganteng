@extends('app-layouts.admin.index')

@section('content')
<div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Data List Kegiatan</h1>
      </div>
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header d-inline">
              <h4>Data Kegiatan</h4>
              <a href="/dashboard/article/create" class=" btn btn-primary mt-3">Tambah Data Kegiatan </a>
              <a href="#" class=" btn btn-primary mt-3">Unduh Data</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                  <thead>
                    <tr>
                      <th class="text-center">
                        No
                      </th>
                      <th>Judul</th>
                      <th>Content</th>
                      <th>Image</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($data as $item)
                    <tr>
                      {{-- <input type="hidden" class="delete_id" value="{{ $item->id }}"> --}}
                      <th class="text-center">{{ $loop->iteration}}</th>
                      <th>{{ $item->title }}</th>
                      <th>{{ $item->body }}</th>
                      <th>
                        <img src="{{ asset('storage/'. $item->image) }}" alt="" class="" style="height: 100px;">
                      </th>
                      <td>
                          <a href="/dashboard/article/{{ $item->id }}/edit" class="btn btn-info">
                          <i class="fa fa-pencil-alt"></i>
                          </a>
                          <a href="{{ route('article.destroy', $item->id) }}" class="btn btn-danger" data-confirm-delete="true">
                            <i class="fa fa-trash"></i>
                          </a>
                      </td>
                  </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="section-body">
      </div>
    </section>
  </div>
@endsection
