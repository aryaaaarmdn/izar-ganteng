@extends('app-layouts.admin.index')

@section('content')
<div class="main-content">
    <section class="section">
      <div class="section-header">
        <h1>Data List Vitamin</h1>
      </div>
      @include('includes.admin.alert')
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header d-inline">
              <h4>Data Vitamin</h4>
              <a href="/dashboard/vitamin/create" class=" btn btn-primary mt-3">Tambah Data Vitamin</a>
              <a href="#" class=" btn btn-primary mt-3">Unduh Data</a>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped" id="table-1">
                  <thead>
                    <tr>
                      <th class="text-center">
                        No
                      </th>
                      <th>Jenis Vitamin</th>
                      <th>Keterangan</th>
                      <th>Maksimal Umur</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($data as $item)
                    <tr>
                      <th class="text-center">{{ $loop->iteration}}</th>
                      <th>{{ $item->jenis_vitamin }}</th>
                      <th>{{ $item->keterangan }}</th>
                      <th>
                        {{ $item->maksimal_umur_tahun_pengguna }} tahun {{ $item->maksimal_umur_bulan_pengguna }} bulan
                      </th>
                      <td>
                          <a href="/dashboard/vitamin/{{ $item->id }}/edit" class="btn btn-info">
                          <i class="fa fa-pencil-alt"></i>
                          </a>
                          <a href="{{ route('vitamin.destroy', $item->id) }}" class="btn btn-danger" data-confirm-delete="true">
                            <i class="fa fa-trash"></i>
                          </a>
                      </td>
                  </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="section-body">
      </div>
    </section>
  </div>
@endsection
