<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Article extends Model
{
    use HasFactory;

    const EXCERPT_LENGTH = 100;

    protected $table = 'article';

    protected $fillable = ['title', 'body', 'image'];

    public function excerpt()
    {
        return Str::limit($this->body, Article::EXCERPT_LENGTH);
    }
}
