<?php

namespace App\Http\Controllers;

use App\Models\Kader;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class KaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Kader::all();
        $sweetalert_delete_title = 'Delete Data Kader!';
        $sweetalert_delete_msg = "Apakah yakin ingin hapus data kader?";
        confirmDelete($sweetalert_delete_title, $sweetalert_delete_msg);
        // return view('users.index', compact('users'));
        return view('kader.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kader.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'umur_tahun' => 'required|numeric',
            'umur_bulan' => [
                'required',
                Rule::in(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11'])
            ],
            'jabatan' => 'required',
            'status' => 'required'
        ]);

        Kader::create($data);
        return redirect('/dashboard/kader')->with('success', 'Data Kader Berhasil di Tambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Kader::findOrFail($id);

        return view('kader.edit', compact('data'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'nama' => 'required',
            'alamat' => 'required',
            'umur_tahun' => 'required|numeric',
            'umur_bulan' => [
                'required',
                Rule::in(['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11'])
            ],
            'jabatan' => 'required',
            'status' => 'required'
        ]);

        $item = Kader::findOrFail($id);

        $item->update($data);
        return redirect('/dashboard/kader')->with('info', 'Data Kader Berhasil di Update');;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Kader::findOrFail($id);

        $item->delete();

        return redirect('/dashboard/kader')->with('success', 'Data Kader Berhasil di Hapus');
    }
}
