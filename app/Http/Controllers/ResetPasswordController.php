<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ResetPasswordController extends Controller
{
    // Halaman verifikasi email
    public function view()
    {
        return view('auth.forgot-password');
    }

    // Proses verifikasi email
    public function verifyEmail(Request $request)
    {
        $akun = User::where('email', $request->email)->firstOr(function(){
            return null;
        });

        if ($akun) {
            // Pake session buat cek apakah diizinkan untuk ganti password
            // session email buat ambil data email yang bakalan diganti passwordnya
            // session is_verified_reset_password buat cek apakah ada izin atau tidak
            session([
                'email' => $request->email,
                'is_verified_reset_password' => true,
            ]);
            return redirect()->route('reset-password.view');
        }

        return redirect()->back()->with('error', 'Terjadi Kesalahan Saat Verifikasi Email');
        
    }

    // Halaman ganti password
    public function resetPasswordView()
    {
        if (session('email') && session('is_verified_reset_password')) {
            return view('auth.custom-reset-password');
        }
        return redirect()
            ->route('login')
            ->with('error_reset_password', 'Tidak ada izin ganti password');
    }

    // Proses ganti password
    public function reset(Request $request)
    {
        if (!session()->has('email') && !session()->has('is_verified_reset_password')) {
            return redirect()
                ->route('login')
                ->with('error_reset_password', 'Tidak ada izin ganti password');
        }

        $request->validate([
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ]);

        $akun = User::where('email', session('email'))->firstOr(function(){
            return null;
        });

        if ($akun) {
            $akun->password = Hash::make($request->password);
            $akun->save();
            $request->session()->forget(['email', 'is_verified_reset_password']);

            return redirect()->route('login')
                ->with('success_reset_password', 'Berhasil Reset Password');
        }
        
        return redirect()
            ->back()
            ->with('error_reset_pass', 'Terjadi Kesalahan Saat Reset Password');
    }
}
