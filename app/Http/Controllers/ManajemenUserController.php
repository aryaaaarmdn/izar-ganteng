<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ManajemenUserController extends Controller
{
    public function index()
    {
        $data = User::all();
        return view('admin.manajemen-user.index', [
            'data' => $data
        ]);
    }

    public function addUser()
    {
        return view('admin.manajemen-user.add');
    }

    public function storeUser(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $data = [
            'name' => $request->nama,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ];

        $newUser = User::create($data);

        if ($newUser) {
            return redirect()->route('user.index')->with('add_success', 'Berhasil menambahkan data user baru');
        } else {
            return redirect()->back()->with('add_err', 'Terjadi kesalahan saat menambahkan data user baru');
        }
    }

    public function deleteUser(Request $request, $id)
    {
        $user = User::where('id', $id)->firstOr(function() {
            return null;
        });

        if (!$user) {
            return redirect()->route('user.index')->with('delete_err', 'Terjadi kesalahan saat hapus data user');
        }

        $user->delete();
        return redirect()->route('user.index')->with('delete_success', 'Berhasil Hapus data user');
    }

    public function ubahPasswordView(User $id)
    {
        return view('admin.manajemen-user.ubah-password', [
            'data' => $id,
        ]);
    }

    public function ubahPassword(Request $request, User $id)
    {
        $request->validate([
            'password' => 'required',
        ]);

        $id->password = Hash::make($request->password);
        $id->save();

        return redirect()->route('user.index')->with('changePwd_success', 'Berhasil ganti password');
    }
}
