<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SuperAdminAuthenticateController extends Controller
{
    public function loginView()
    {
        return view('admin.login');
    }


    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $kredensial = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (Auth::guard('superadmin')->attempt($kredensial)) {
            $request->session()->regenerate();
 
            return redirect()->route('admin.index');
        } else {
            return redirect()->back()->with('error_login', 'Email / Password salah');
        }
    }

    public function logout(Request $request)
    {
        Auth::guard('superadmin')->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }
}
