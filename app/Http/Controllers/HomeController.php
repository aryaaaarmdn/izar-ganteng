<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        // ambil semua artikel urut dari yang terbaru
        $data = Article::latest()->get();
        return view('home', compact('data'));
    }
}
