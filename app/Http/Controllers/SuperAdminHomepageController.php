<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class SuperAdminHomepageController extends Controller
{
    public function index()
    {
        $total_user = User::count();
        return view('admin.dashboard', [
            'data' => $total_user
        ]);
    }
}
